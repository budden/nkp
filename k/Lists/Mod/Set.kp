MODULE ListsSet;
(**
	project	= "Lists"
	organization	= ""
	contributors	= ""
	version	= "System/Rsrc/About"
	copyright	= "Kushnir Piotr Michailovich"
	license	= "Docu/BB-License"
	purpose	= "Наборы аналогичные типу SET, но с указателями"
	changes	= "
	- 20130723, pk, автогенерация заголовка
"
	issues	= ""
**)

	IMPORT
		ListsLinear;
		
	TYPE
		Set* = POINTER TO ABSTRACT RECORD 
			inverted-: BOOLEAN;
		END;
		
		StdSet = POINTER TO RECORD (Set) 
			list: ListsLinear.List;
			c: Comparator;
		END;
		
		Item = POINTER TO RECORD (ListsLinear.ListItem) 
			data: ANYPTR;
		END;
		
		Data* = POINTER TO ABSTRACT RECORD END;
		
		Comparator* = POINTER TO ABSTRACT RECORD END;
		
	PROCEDURE (s: Set) Incl* (x: ANYPTR), NEW, ABSTRACT;
	PROCEDURE (s: Set) Excl* (x: ANYPTR), NEW, ABSTRACT;
	PROCEDURE (s: Set) In* (x: ANYPTR): BOOLEAN, NEW, ABSTRACT;
	PROCEDURE (s: Set) NofItems* (): INTEGER, NEW, ABSTRACT;
	PROCEDURE (s: Set) Item* (idx: INTEGER): ANYPTR, NEW, ABSTRACT;

	PROCEDURE (s: Set) SetInversion* (inv: BOOLEAN), NEW, EXTENSIBLE;
	BEGIN
		s.inverted:=inv;
	END SetInversion;
	
	PROCEDURE (d: Data) EqualTo* (x: Data): BOOLEAN, NEW, ABSTRACT;
	
	PROCEDURE (c: Comparator) Equal* (a, b: ANYPTR): BOOLEAN, NEW, ABSTRACT;
	
	PROCEDURE ItemOf (s: StdSet; data: ANYPTR; OUT idx: INTEGER): Item;
		VAR x, y: ANYPTR; res: Item; i: INTEGER;
	BEGIN
		ASSERT(s#NIL, 21); ASSERT(data#NIL, 20);
		i:=0; idx:=-1;
		WHILE (i<s.list.len) & (res=NIL) DO
			x:=s.list.GetItem(i);
			WITH x: Item DO 
				y:=x.data;
				ASSERT(y#NIL, 40);
				IF (s.c#NIL) & s.c.Equal(data, y) THEN res:=x
				ELSIF (data IS Data) & (y IS Data) THEN
					IF (y(Data).EqualTo(data(Data))) THEN res:=x END;
				ELSIF x.data=data THEN res:=x END
			ELSE HALT(100) END;
			IF res#NIL THEN idx:=i END;
			INC(i)
		END;
	RETURN res
	END ItemOf;
	
	PROCEDURE Grow (s: StdSet): Item;
		VAR res: Item;
	BEGIN
		NEW(res);
		s.list.SetLength(s.list.len+1);
		s.list.SetItem(s.list.len-1, res);
	RETURN res
	END Grow;
	
	PROCEDURE (s: StdSet) NofItems(): INTEGER;
	BEGIN
	RETURN s.list.len
	END NofItems;
	
	PROCEDURE (s: StdSet) Item (idx: INTEGER): ANYPTR;
		VAR x, res: ANYPTR;
	BEGIN
		ASSERT(idx>=0, 20); ASSERT(idx<s.list.len);
		x:=s.list.GetItem(idx);
		ASSERT(x#NIL, 40);
		WITH x: Item DO res:=x.data ELSE HALT(100) END;
	RETURN res
	END Item;
	
	PROCEDURE (s: StdSet) Incl (x: ANYPTR);
		VAR i: Item; idx: INTEGER;
	BEGIN
		ASSERT(x#NIL, 20);
		i:=ItemOf(s, x, idx);
		IF i=NIL THEN 
			i:=Grow(s);
			i.data:=x;
		END;
	END Incl;
	
	PROCEDURE (s: StdSet) Excl (x: ANYPTR);
		VAR i: Item; idx: INTEGER;
	BEGIN
		ASSERT(x#NIL, 20);
		i:=ItemOf(s, x, idx);
		IF i#NIL THEN
			s.list.Remove(idx)
		END;
	END Excl;
	
	PROCEDURE (s: StdSet) In (x: ANYPTR): BOOLEAN;
		VAR item: Item; idx: INTEGER; xok: BOOLEAN;
	BEGIN
		ASSERT(x#NIL, 20);
		item:=ItemOf(s, x, idx);
		xok:=((item#NIL) # s.inverted); (* инверсия результата наличия элемента *)
	RETURN xok;
	END In;
	
	PROCEDURE New*(): Set;
		VAR s: StdSet;
	BEGIN
		NEW(s); s.list:=ListsLinear.NewList(); s.inverted:=FALSE;
	RETURN s
	END New;
	
	PROCEDURE Copy*(s: Set): Set;
		VAR c: Set; i, len: INTEGER; x: ANYPTR;
	BEGIN
		ASSERT(s#NIL, 20);
		c:=New(); c.inverted:=s.inverted;
		i:=0; len:=s.NofItems();
		WHILE i<len DO
			x:=s.Item(i);
			ASSERT(x#NIL, 40);
			c.Incl(x);
			INC(i)
		END;
	RETURN c;
	END Copy;
	
	PROCEDURE Equal* (a, b: Set): BOOLEAN;
		VAR ok: BOOLEAN; i, len: INTEGER; x: ANYPTR;
	BEGIN
		ASSERT(a#NIL, 20); ASSERT(b#NIL, 21);
		ok:=(a.NofItems()=b.NofItems()) & (a.inverted=b.inverted);
		IF ok THEN
			i:=0; len:=a.NofItems();
			WHILE (i<len) & ok DO
				x:=a.Item(i);
				ASSERT(x#NIL, 40);
				ok:=b.In(x);
				INC(i)
			END;
		END;
	RETURN ok
	END Equal;
	
	PROCEDURE Sum* (a, b: Set): Set;
		VAR c: Set; i, len: INTEGER; x: ANYPTR;
	BEGIN
		ASSERT(a#NIL, 20); ASSERT(b#NIL, 21);
		c:=New();
		i:=0; len:=a.NofItems();
		WHILE (i<len) & ~a.inverted DO
			x:=a.Item(i);
			ASSERT(x#NIL, 40);
			c.Incl(x);
			INC(i);
		END;
		i:=0; len:=b.NofItems();
		WHILE (i<len) & ~b.inverted DO
			x:=b.Item(i);
			ASSERT(x#NIL, 40);
			c.Incl(x);
			INC(i);
		END;
	RETURN c
	END Sum;
	
	PROCEDURE Mult* (a, b: Set): Set;
		VAR c: Set; i, len: INTEGER; x: ANYPTR;
	BEGIN
		ASSERT(a#NIL, 20); ASSERT(b#NIL, 21);
		c:=New();
		i:=0; len:=a.NofItems();
		WHILE (i<len) & ~a.inverted DO
			x:=a.Item(i);
			ASSERT(x#NIL, 40);
			IF b.In(x) THEN c.Incl(x) END;
			INC(i);
		END;
		i:=0; len:=b.NofItems();
		WHILE (i<len) & ~b.inverted DO
			x:=b.Item(i);
			ASSERT(x#NIL, 40);
			IF a.In(x) THEN c.Incl(x) END;
			INC(i);
		END;
	RETURN c
	END Mult;
		
	PROCEDURE Invert* (s: Set): Set;
		VAR c: Set; 
	BEGIN
		ASSERT(s#NIL, 20);
		c:=Copy(s);
		c.inverted:=~c.inverted;
	RETURN c
	END Invert;
	
	PROCEDURE Diff* (a, b: Set): Set;
		VAR c: Set;
	BEGIN
		c:=Mult(a, Invert(b));
	RETURN c
	END Diff;
	
	PROCEDURE Div* (a, b: Set): Set;
		VAR c: Set;
	BEGIN
		c:=Sum(Diff(a, b), Diff(b, a));
	RETURN c
	END Div;
	
	PROCEDURE SetComp*(s: Set; c: Comparator);
	BEGIN
		ASSERT(s#NIL, 20);
		WITH s: StdSet DO
			s.c:=c
		ELSE HALT(100) END;
	END SetComp;
	
END ListsSet.

ListsObxSet.Do