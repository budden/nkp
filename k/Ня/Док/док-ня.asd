;;; -*- coding: utf-8; Mode: Lisp -*-

;; Для пересоздания док-та выполни (ОЯ-ИНФРАСТРУКТУРА:Собрать-проект-Html-файла :ДОК-НЯ)

(in-package #:asdf)
(named-readtables::in-readtable :buddens-readtable-a)

(defsystem :ДОК-НЯ
 :serial t
 :depends-on (:СПРАВОЧНИКИ-HTML)
 :components
  ((:file "пакет-док-ня")
   (:file "очистить-БД-и-заполнить-теги")
   (:file "док-ня-бд")
   ))

